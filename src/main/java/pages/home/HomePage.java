package pages.home;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pages.base.BasePage;

import java.util.List;

import static org.openqa.selenium.Keys.ENTER;

public class HomePage extends BasePage {

    public HomePage(WebDriver driver) {
        super(driver);
    }

    /**
     * спан города в шапке сайта
     */
    By spanCity = By.xpath("//span[@class='city__name city__btn city__name--label']");

    /**
     * кнопка Выбрать другой
     */
    By selectBasketCity = By.xpath("//a[@href='#selectBasketCity']");

    /**
     * поле ввода поиска города
     */
    By inputSearchCity = By.xpath("//input[@class='change-city-search-input']");

    /**
     * лист результатов
     */
    By resultCityList = By.xpath("//div[@class='easy-autocomplete-container']//li");    //лист результатов поиска городо

    /**
     * кнопка пациентам (но при клике меняется на то что выбрал)
     */
    By patient = By.xpath("//div[@class='invitro_header-bottom_left']//div[@class='invitro_header-target_audience']");  //Пациентам на главной станице

    /**
     * лист разделов при клике по пациентам
     */
    By patientList= By.xpath("//div[@class='mfp-content']//a[contains(@class,'invitro_header-target_audienc')]");//Лист 'пациентам' на главной транице

    /**
     * кнопка результаты анализов
     */
    By resultButton = By.xpath("//div[@class='invitro_header-bottom_right']//a[@class='invitro_header-get_result']");

    /**
     * "Поиск на сайте" в шапке
     */
    By inputSeach = By.xpath("//input[@placeholder='Поиск на сайте']");

    /**
     * список номеров продуктов
     */
    By resultForSeachNumberAnalizesList = By.xpath("//div[@class='search__result-number']");


    public void clickPatient(){
        waitElementIsVisible(driver.findElement(patient));
        driver.findElement(patient).click();
    }

    public void clickSpanCity(){
        waitElementIsVisible(driver.findElement(spanCity));
        driver.findElement(spanCity).click();
    }

    public void clickSelectBasketCity(){
        waitElementIsVisible(driver.findElement(selectBasketCity));
        driver.findElement(selectBasketCity).click();
    }

    public void inputCity(String value){
        waitElementIsVisible(driver.findElement(inputSearchCity));
        driver.findElement(inputSearchCity).sendKeys(value);
    }

    public void selectClick(String value){
        waitElementIsVisible(driver.findElement(resultCityList));
        List<WebElement> city_list = driver.findElements(resultCityList);
        for (WebElement city : city_list){
            if (city.getText().equals(value)){
                city.click();
                return;
            }
        }
        Assert.fail("Не найден " + value);
    }

    public void checkCity(String value){
        waitElementIsVisible(driver.findElement(spanCity));
        Assert.assertEquals(driver.findElement(spanCity).getText(),value);
    }

    public void selectPatient(String value){
        waitElementIsVisible(driver.findElement(patientList));
        List<WebElement> patient_list = driver.findElements((patientList));
        for (WebElement patient : patient_list){
            if (patient.getText().equals(value)){
                patient.click();
                return;
            }
        }
        Assert.fail("Не найден раздел " + value);

    }

    public void inputSeachSendkeys(String analizs){
        waitElementIsVisible(driver.findElement(inputSeach));
        driver.findElement(inputSeach).sendKeys(analizs);
        driver.findElement(inputSeach).sendKeys(ENTER);
    }

    public void checkSeach(String analizs){
        waitElementIsVisible(driver.findElement(resultForSeachNumberAnalizesList));
        Assert.assertTrue(driver.findElement(resultForSeachNumberAnalizesList).getText().contains(analizs));
    }

    public void clickResultButton(){
        waitElementIsVisible(driver.findElement(resultButton));
        driver.findElement(resultButton).click();
    }





}
