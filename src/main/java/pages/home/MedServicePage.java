package pages.home;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import pages.base.BasePage;

import java.util.List;

public class MedServicePage extends BasePage {

    public MedServicePage(WebDriver driver) {
        super(driver);
    }

    String menu = "//ul[@class='side-bar-second__list']/li";
    String subMenuItemString = "/./div//li";

    public void clickOnMenu() {
        int menuSize = driver.findElements(By.xpath(menu)).size();
        for(int i = 1; i <= menuSize; i++) {
            String nextItemString = menu + "[" + i + "]";
            By nextMenuItem = By.xpath(nextItemString);
            driver.findElement(nextMenuItem).click();
            WebElement nextLi = driver.findElement(nextMenuItem);
            if(nextLi.findElements(By.xpath("./div//li")).size() > 0) {
                By nextMenuBy = By.xpath(nextItemString + subMenuItemString);
                int subMenuSize = driver.findElements(nextMenuBy).size();
                for(int j = 1; j <= subMenuSize; j++) {
                    String nextSubItemString = nextItemString + subMenuItemString + "[" + j + "]";
                    By nextSubMenuItem = By.xpath(nextSubItemString);
                    driver.findElement(nextSubMenuItem).click();
                }
            }
        }
    }

}

