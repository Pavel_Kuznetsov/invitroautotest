package pages.home;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pages.base.BasePage;

import java.util.List;

public class NoRegistrationResultsPage extends BasePage {

    public NoRegistrationResultsPage(WebDriver driver) {
        super(driver);
    }

    /**
     * элемент h2. заголовок на странице
     */
    By h2 = By.xpath("//h2");

    /**
     *  кнопка найти рзультаты
     */
    By buttonSearchResults = By.xpath("//button[contains(text(),'Найти результаты')]");

    /**
     * текст ошибки
     */
    By errorText = By.xpath("//div[contains(@class,'UnauthResultsPage_error')]");

    /**
     * лист названий полей ввода
     */
    By nameInputList = By.xpath("//div[contains(@class,'UnauthResultsPage_searchFormBlockItem')]/label");

    /**
     * поле ввода
     */
    By inputList = By.xpath("./parent::div//input");


    public void checkH2(String text){
        waitElementIsVisible(driver.findElement(h2));
        Assert.assertEquals(driver.findElement(h2).getText(),text);
    }

    public void clickButtonSearchResults(){
        waitElementIsVisible(driver.findElement(buttonSearchResults));
        driver.findElement(buttonSearchResults).click();
    }

    public void checkErrorText(String text){
        waitElementIsVisible(driver.findElement(errorText));
        Assert.assertEquals(driver.findElement(errorText).getText(),text);
    }

    public void checkRed(){
        waitElementIsVisible(driver.findElement(nameInputList));
        List<WebElement> name_list = driver.findElements(nameInputList);
        for (WebElement nameTitle : name_list){
            if (!nameTitle.findElement(inputList)
                    .getCssValue("border-color").equals("rgb(255, 0, 0)")
            & !nameTitle.findElement(inputList)
                    .getCssValue("color").equals("rgba(255, 0, 0, 1)")){
                Assert.fail("Поле " +nameTitle.getText() +" не подсечено красным");
            }
        }
    }



    public void inputField(String nameInput, String text){
        waitElementIsVisible(driver.findElement(nameInputList));
        List<WebElement> name_list = driver.findElements(nameInputList);
        for (WebElement name_ : name_list){
            if (name_.getText().equals(nameInput)){
                name_.findElement(inputList).click();
                name_.findElement(inputList).clear();
                name_.findElement(inputList).sendKeys(text);
                return;
            }
        }
        Assert.fail("Не найдено поле " + nameInput);
    }

    public void checkField(String nameInput, String text){
        waitElementIsVisible(driver.findElement(nameInputList));
        List<WebElement> name_list = driver.findElements(nameInputList);
        for (WebElement name_ : name_list){
            if (name_.getText().equals(nameInput)){
                Assert.assertEquals(name_.findElement(inputList).getAttribute("value"),text);
                return;
            }
        }
        Assert.fail("Не найдено поле " + nameInput);
    }

}
