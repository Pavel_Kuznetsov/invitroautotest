package pages.home;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pages.base.BasePage;

import java.util.List;

public class AnalizesPage extends BasePage {

    public AnalizesPage(WebDriver driver) {
        super(driver);
    }

    /**
     * //лист номеров анализов
     */
    By nameAnalizes = By.xpath("//span[@class='result-item__number']/span");

    /**
     * //лист цен
     */
    By spanPrice = By.xpath("//span[@class='result-item__price']");

    /**
     * лист кнопок в корзину
     */
    By buttonBasket = By.xpath("//div[contains(@class,'btn-icon btn-icon--fill btn-cart add_basket')]");

    /**
     * иконка для перехода в корзину
     */
    By basket = By.xpath("//div[@class='invitro_header-menu']//div[@class='invitro-header-cart__icon']/a");

    /**
     * лист номеров анализов в корзине
     */
    By lineforbasket = By.xpath("//div[contains(@class,'CartProduct_productBottom')]");



    static String price;
    static String name;


    public static String getPrice() {
        return price;
    }

    public static void setPrice(String price) {
        AnalizesPage.price = price;
    }

    public static String getName() {
        return name;
    }

    public static void setName(String name) {
        AnalizesPage.name = name;
    }



    public void saveSum(){
        // запоминаем сумму продукта
        waitElementIsVisible(driver.findElements(spanPrice).get(0));
        setPrice(driver.findElements(spanPrice).get(0).getText());
    }

    public void saveNumberAnalizes(){
        // запоминаю номер продукта
        waitElementIsVisible(driver.findElements(nameAnalizes).get(0));
        setName(driver.findElements(nameAnalizes).get(0).getText());
    }

    public void addAnalizes(){
        waitElementIsVisible(driver.findElement(buttonBasket));
        driver.findElement(buttonBasket).click();
    }


    public void goToBasket(){
        waitElementIsVisible(driver.findElement(basket));
        driver.findElement(basket).click();
    }


    public void checkSum(){
        waitElementIsVisible(driver.findElement(lineforbasket));
        List<WebElement> lineList = driver.findElements(lineforbasket);
        for (WebElement line : lineList){

            if (line.getText().contains(getName())){
                String sumForBasket = line.findElement(By.xpath("./parent::div//div[contains(@class,'CartProduct_productRight')]")).getText();

                Assert.assertEquals(getPrice().replaceAll("руб","")
                        ,sumForBasket.replaceAll("₽",""));
                return;
            }
        }
        Assert.fail("Не найден продукт" + getName() +" в корзине ");

    }

}
