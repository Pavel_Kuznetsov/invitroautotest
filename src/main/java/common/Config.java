package common;

public class Config {

    public static final String PLATTFORM_AND_BROWSER = "win_chrome";

    /**
     * if true - очищаем куки и историю
     */
    public static final Boolean CLERAR_COOKIS_AND_STORAGE = true;

    /**
     * if true - закрывает браузер
     */
    public static final Boolean HOLD_BROWSER_OPEN = true;


}
