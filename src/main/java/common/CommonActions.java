package common;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

import static common.Config.PLATTFORM_AND_BROWSER;
import static constans.Constant.TimeoutVariable.IMPLICIT_WAIT;

public class CommonActions {


    public static WebDriver createDriver(){
        WebDriver driver = null;
        switch (PLATTFORM_AND_BROWSER) {
            case "win_chrome":
                System.setProperty("webdriver.chrome.driver","src/main/resources/chromedriver.exe");
                driver = new ChromeDriver();
                break;
            default:
                Assert.fail("incorrect platform or browser name: " + PLATTFORM_AND_BROWSER);
        }
        driver.manage().window().maximize(); //окно при открытие будет на весь экран
        driver.manage().timeouts().implicitlyWait(IMPLICIT_WAIT, TimeUnit.SECONDS);
        return driver;
    }
}
