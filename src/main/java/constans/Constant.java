package constans;

public class Constant {
    public static class TimeoutVariable{
        public static final int IMPLICIT_WAIT = 4 ;
        public static final int EXPLICIT_WAIT = 10 ;

    }

    public static class URLVariable{

        public static final String REAL_HOME_PAGE = "https://www.invitro.ru/";
    }
}
