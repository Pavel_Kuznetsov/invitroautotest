package Task;

import base.BaseTest;
import org.testng.annotations.Test;

import static constans.Constant.URLVariable.REAL_HOME_PAGE;

public class Task3RezultAnalizesTest extends BaseTest {

    @Test
    public void check(){
        basePage.open(REAL_HOME_PAGE);
        homePage.clickResultButton();
        noRegistrationResultsPage.checkH2("Введите индивидуальный номер заказа, чтобы посмотреть результаты анализов");
        noRegistrationResultsPage.clickButtonSearchResults();
        noRegistrationResultsPage.checkErrorText("Поля Код ИНЗДата рожденияФамилия обязательны для заполнения");
        noRegistrationResultsPage.checkRed();
        noRegistrationResultsPage.inputField("Код ИНЗ","999888999");
        noRegistrationResultsPage.inputField("Дата рождения","01.01.1999");
        noRegistrationResultsPage.inputField("Фамилия","Иванов");
        noRegistrationResultsPage.checkField("Код ИНЗ","999888999");
        noRegistrationResultsPage.checkField("Дата рождения","01.01.1999");
        noRegistrationResultsPage.checkField("Фамилия","Иванов");

    }
}
