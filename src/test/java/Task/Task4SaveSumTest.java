package Task;

import base.BaseTest;
import org.testng.annotations.Test;

public class Task4SaveSumTest extends BaseTest {

    @Test
    public void checkSumma(){
        basePage.open("https://www.invitro.ru/analizes/for-doctors/");
        analizesPage.saveSum();
        analizesPage.saveNumberAnalizes();
        analizesPage.addAnalizes();
        analizesPage.goToBasket();
        analizesPage.checkSum();
    }
}
