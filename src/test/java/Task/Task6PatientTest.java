package Task;

import base.BaseTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import static constans.Constant.URLVariable.REAL_HOME_PAGE;

public class Task6PatientTest extends BaseTest {

    @DataProvider(name = "menuItem")
    public static Object[] evenMenuItem() {
        return new Object[][]{ {"Врачам"}, {"Франчайзинг"}, {"Корпоративным клиентам"}, {"Прессе"}};
    }

    @Test(dataProvider = "menuItem")
    public void checkPatient(String value){
        basePage.open(REAL_HOME_PAGE);
        homePage.clickPatient();
        homePage.selectPatient(value);
    }

    @Test
    @Parameters({"valueTask6"})
    public void checkPatient2(String value){
        basePage.open(REAL_HOME_PAGE);
        homePage.clickPatient();
        homePage.selectPatient(value);
    }

}
