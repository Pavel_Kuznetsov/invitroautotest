package Task;

import base.BaseTest;
import org.testng.annotations.Test;
import static constans.Constant.URLVariable.REAL_HOME_PAGE;

public class Task2ChangeCityTest extends BaseTest {

    @Test
    public void checkCity(){
        basePage.open(REAL_HOME_PAGE);
        homePage.clickSpanCity();
        homePage.clickSelectBasketCity();
        homePage.inputCity("Омск");
        homePage.selectClick("Омск");
        homePage.checkCity("Омск");
    }

}
