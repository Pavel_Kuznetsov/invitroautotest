package base;


import common.CommonActions;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import pages.base.BasePage;
import pages.home.AnalizesPage;
import pages.home.HomePage;
import pages.home.MedServicePage;
import pages.home.NoRegistrationResultsPage;

import static common.Config.CLERAR_COOKIS_AND_STORAGE;
import static common.Config.HOLD_BROWSER_OPEN;


public class BaseTest {
    protected WebDriver driver = CommonActions.createDriver();
    protected BasePage basePage = new BasePage(driver);
    protected HomePage homePage = new HomePage(driver);
    protected MedServicePage medServicePage  = new MedServicePage(driver);
    protected AnalizesPage analizesPage  = new AnalizesPage(driver);
    protected NoRegistrationResultsPage noRegistrationResultsPage  = new NoRegistrationResultsPage(driver);

    @AfterTest
    public void clearCookieAndLocalStorage(){
        if(CLERAR_COOKIS_AND_STORAGE) {
            JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;
            driver.manage().deleteAllCookies();
            //javascriptExecutor.executeAsyncScript("window.sessionStorage.clear()");
        }
    }

    /**
     * If true - то после каждого теста закрывать браузер
     */
    @AfterSuite(alwaysRun = true)
    public void close(){
        if(HOLD_BROWSER_OPEN) {
            driver.quit();
        }
    }
}
